# frozen_string_literal: true

class Test
  def hello
    'hello world'
  end
end
