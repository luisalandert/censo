# frozen_string_literal: true

require 'spec_helper'

describe Test do
  context '#hello' do
    it 'returns hello' do
      test = Test.new
      response = test.hello
      expect(response).to eq('hello world')
    end
  end
end
